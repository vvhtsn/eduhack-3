﻿ using UnityEngine;
 using System.Collections;
 
 public class WorldSpawn : MonoBehaviour {
     
     public GameObject block1; 
 
     public int worldWidth  = 10;
     public int worldHeight  = 10;
 
     public float spawnSpeed = 0;


     public Material mat;
     void  Start ()
     {
         if (block1 == null)
             block1 = GameObject.CreatePrimitive(PrimitiveType.Cube);
         
         CreateWorld();
     }
 
     void CreateWorld () {
         for(int x = 0; x < worldWidth; x++) {
             for(int y = 0; y < worldHeight; y++) {           
                 GameObject block = Instantiate(block1, Vector3.zero, block1.transform.rotation);
                 block.transform.parent = transform;
                 float objectScale = block.transform.localScale.x;
                 block.transform.localPosition = new Vector3(x*objectScale, y*objectScale, 0);
             }
         }
     }

     void CombineMeshes()
     {
         MeshFilter[] meshFilters = GetComponentsInChildren<MeshFilter>();
         CombineInstance[] combine = new CombineInstance[meshFilters.Length];
         int i = 0;
         while (i < meshFilters.Length) {
             combine[i].mesh = meshFilters[i].sharedMesh;
             combine[i].transform = meshFilters[i].transform.localToWorldMatrix;
             meshFilters[i].gameObject.SetActive(false);
             i++;
         }
         transform.GetComponent<MeshFilter>().mesh = new Mesh();
         transform.GetComponent<MeshFilter>().mesh.CombineMeshes(combine);
         transform.GetComponent<MeshRenderer>().material = mat;
         transform.gameObject.SetActive(true);
     }
 }