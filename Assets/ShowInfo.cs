﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShowInfo : MonoBehaviour
{
	public GameObject InfoPanel;
	
	public void ToggleInfo()
	{
		if (InfoPanel.active)
		{
			InfoPanel.SetActive(false);
			return;
		}
		InfoPanel.SetActive(true);
	}

	private void OnMouseDown()
	{
		ToggleInfo();
	}
}
