﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.UIElements;

public class TurbidityControl : MonoBehaviour
{
	public ParticleSystem Particles;


	public void SetParticles()
	{
		var em = Particles.emission;
		em.rateOverTime = GetComponentInChildren<Slider>().value;
	}
	
}
