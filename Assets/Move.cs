﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Move : MonoBehaviour {

	public List<GameObject> Waypoints = new List<GameObject>();

    private int waypointNum;
    float t;
    Vector3 startPosition;
    Vector3 target;
    public float timeToReachTarget;
    
    void Start()
    {
        waypointNum = Random.Range(0, Waypoints.Count);
        SetDestination(Waypoints[waypointNum].transform.position);
        StartCoroutine(MoveToPosition());
    }
    
    public IEnumerator MoveToPosition()
    {
        var currentPos = transform.position;
        var t = 0.2f;
        while(t < 1)
        {
            t += Time.deltaTime / timeToReachTarget;
            transform.position = Vector3.Lerp(currentPos, target, t);
            yield return null;
        }
        SetDestination(Waypoints[waypointNum].transform.position);
        StartCoroutine(MoveToPosition());
    }
    public void SetDestination(Vector3 destination)
    {
        waypointNum++;
        if (waypointNum >= Waypoints.Count)
            waypointNum = 0;
        t = 0;
        startPosition = transform.position;
        target = destination; 
    }
  
}
