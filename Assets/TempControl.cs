﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TempControl : MonoBehaviour
{
	public Image WarmPic;

	public Image ColdPic;
	
	// Update is called once per frame
	public void ChangeColor () {
		var tempColor = WarmPic.color;
		tempColor.a = GetComponentInChildren<Slider>().value;
		WarmPic.color = tempColor;
		
		tempColor = ColdPic.color;
		tempColor.a *= -1;
		ColdPic.color = tempColor;

	}
}
