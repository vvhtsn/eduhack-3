﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamControl : MonoBehaviour {
	public Shader shader;
	// Use this for initialization
	void Start () {
		if (shader)
			transform.GetComponent<Camera>().SetReplacementShader(shader, null);
	}
	
}
